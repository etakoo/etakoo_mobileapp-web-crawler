package ma.hatim.ahadri.pricecrawler.model;

import java.io.Serializable;

/**
 * Created by hatim on 06/04/16.
 */

public class User implements Serializable
{
    private String email ;
    private String full_name ;
    private String password ;

    public User() {
        super();
    }
    public User(String email, String full_name, String password)
    {
        super();
        this.email = email;
        this.full_name = full_name;
        this.password = password;
    }
    /** The getters and the setters !**/
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getFull_name() {
        return full_name;
    }
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

}
