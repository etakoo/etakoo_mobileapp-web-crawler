package ma.hatim.ahadri.pricecrawler.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.model.Category;
import ma.hatim.ahadri.pricecrawler.model.Product;
import ma.hatim.ahadri.pricecrawler.service.CategoryService;

/**
 * Created by ahadri on 11/12/16.
 */

public class Util {

	private static  Util instance;

	public static Util getInstance() {
		if (instance == null)
				instance = new Util();
		return instance;
	}

	public Util() {
	}

	private final String TAG = "Util";
	private static final List<String> categoriesName = new ArrayList<>();
	static {
		categoriesName.add("tv_video_audio");
		categoriesName.add("fashion");
		categoriesName.add("mobile");
		categoriesName.add("health_beauty");
		categoriesName.add("computing");
		categoriesName.add("gaming");
	}
	public List<Category> getCategories(JSONArray suppliers) {
		try {
			List<Category> categories = initCategories();
			List<Product> products;
			for (int i = 0; i < suppliers.length(); i++) {
				JSONObject supplier = suppliers.getJSONObject(i);
				String supplierName = supplier.keys().next();
				JSONArray supplierCategories = supplier.getJSONArray(supplierName);
				for (int j = 0; j < supplierCategories.length(); j++) {
					JSONObject categoryJson = supplierCategories.getJSONObject(j);
					String categoryName = categoryJson.keys().next();
					JSONArray productsJson = new JSONArray(categoryJson.getString(categoryName));
					products = new ArrayList<>();
					for(int k = 0; k < productsJson.length(); k++) {
						JSONObject productJson = productsJson.getJSONObject(k);
						Product product = new Product();
						product.setTitle(productJson.getString("title"));
						product.setPrice(productJson.getString("price"));
						String urlJson = productJson.getString("url");
						String url = urlJson.substring(0, urlJson.length());
						product.setUrl(url);
						product.setSupplier(productJson.getString("supplier"));
						String urlImgJson = productJson.getString("urlImg");
						String urlImg = urlImgJson.substring(0, urlImgJson.length());
						product.setImgUrl(urlImg);
						products.add(product);
					}
					getCategoryByName(categories, categoryName).pushProducts(products);
				}
			}
				return categories;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();

	}
	private List<Category> initCategories() {
		List<Category> categories = new ArrayList<>();

		for(String categoryName: categoriesName) {
			Category category = new Category(categoryName, new ArrayList<Product>());
			categories.add(category);
		}
		return categories;
	}
	public Category getCategoryByName(List<Category> categories, String categoryName) {
		for(Category category : categories) {
			if (category.getCategoryName().equals(categoryName) ) {
				return category;
			}
		}
		return new Category();
	}


	public List<Product> getProducts(JSONArray productsJson) {
		List<Product> products = new ArrayList<>();
		int i;
		for( i = 0; i < productsJson.length(); i++) {
			Product product = new Product();
			try {
				product.setTitle(productsJson.getJSONObject(i).getString("title"));
				product.setPrice(productsJson.getJSONObject(i).getString("price"));
				String urlJson = productsJson.getJSONObject(i).getString("url");
				String url = urlJson.substring(0, urlJson.length());
				product.setUrl(url);
				product.setSupplier(productsJson.getJSONObject(i).getString("supplier"));
				String urlImgJson = productsJson.getJSONObject(i).getString("urlImg");
				String urlImg = urlImgJson.substring(0, urlImgJson.length());
				product.setImgUrl(urlImg);
				products.add(product);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return products;
	}






}
