package ma.hatim.ahadri.pricecrawler.service;

import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import ma.hatim.ahadri.pricecrawler.config.Config;
import ma.hatim.ahadri.pricecrawler.model.Product;
import ma.hatim.ahadri.pricecrawler.util.Util;

/**
 * Created by ahadri on 14/12/16.
 */

public class ProductService {

	private Util util = Util.getInstance();
	private String TAG = this.getClass().getSimpleName();



	private static ProductService instance;
	public static ProductService getInstance() {
		if (instance == null) {
			instance = new ProductService();
		}
		return instance;
	}


	public ProductService() {}

	public ProductService(Util util) {
		this.util = util;
	}

	public List<Product> getSearchProducts(String userQuery)
	{
		OkHttpClient httpClient = new OkHttpClient();
		String get_query = Config.SEARCH + userQuery;
				Request request = new Request.Builder()
				.url(get_query)
				.build();
		List<Product> products = new ArrayList<>();
		try {
			Response response = httpClient.newCall(request).execute();
				if (response.code() == 200) {
				String responseData = response.body().string();
				JSONObject responseJson = new JSONObject(responseData);
				JSONArray productsJson = responseJson.getJSONArray("msg");
				products = util.getProducts(productsJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return products;
	}


	public HashMap<String, List<String>> getProductDetails(String productSupplier, String productUrl)
	{
		Log.i(TAG, "Supplier : " + productSupplier + ", Url : " + productUrl);
		OkHttpClient httpClient = new OkHttpClient();

		RequestBody postArgs = new FormEncodingBuilder()
				.add("supplier", productSupplier)
				.add("url", productUrl)
				.build();


		Request request = new Request.Builder()
				.url(Config.PRODUCT_DETAILS)
				.post(postArgs)
				.build();
		List<String> imgsUrl = new ArrayList<>();
		List<String> details = new ArrayList<>();
		HashMap<String, List<String>> desc = new HashMap<>();
		try {
			Response response = httpClient.newCall(request).execute();
			if (response.code() == 200) {
				String responseData = response.body().string();
				JSONObject responseJson = new JSONObject(responseData);
				JSONObject productDescJson = responseJson.getJSONObject("msg");
				JSONArray detailsJson = productDescJson.getJSONArray("details");
				JSONArray imgsUrlsJson = productDescJson.getJSONArray("imgs");
				int i;
				for(i = 0; i < imgsUrlsJson.length(); i++) {
					String imgUrl = (String ) imgsUrlsJson.get(i);
					imgsUrl.add(imgUrl);
				}
				for(i = 0; i < detailsJson.length(); i++) {
					String detail = (String ) detailsJson.get(i);
					details.add(detail);
				}
				desc.put("details", details);
				desc.put("imgs", imgsUrl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return desc;

	}


}
