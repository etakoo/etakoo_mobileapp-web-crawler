package ma.hatim.ahadri.pricecrawler.service;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import ma.hatim.ahadri.pricecrawler.model.Category;
import ma.hatim.ahadri.pricecrawler.config.Config;
import ma.hatim.ahadri.pricecrawler.util.Util;

/**
 * Created by ahadri on 11/12/16.
 */
public class CategoryService {

	private static CategoryService instance;
	public static CategoryService getInstance() {
		if (instance == null)
			instance = new CategoryService();
		return instance;
	}

	public CategoryService() {
	}

	private List<Category> categories;

	private Util util = Util.getInstance();
	private String TAG = this.getClass().getSimpleName();

	public List<Category> getCategories() {
			OkHttpClient httpClient = new OkHttpClient();
			Request request = new Request.Builder()
					.url(Config.RECOMMEND)
					.build();

			try {
				Response response = httpClient.newCall(request).execute();
				if (response.code() == 200) {
						String responseData = response.body().string();
						JSONObject responseJson = new JSONObject(responseData);
						JSONArray suppliers = responseJson.getJSONArray("msg");
						categories = util.getCategories(suppliers);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
		return categories;
	}

}
