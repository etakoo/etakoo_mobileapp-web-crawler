package ma.hatim.ahadri.pricecrawler.activity.category;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.activity.authentication.RegisterActivity;
import ma.hatim.ahadri.pricecrawler.activity.search.SearchProductActivity;
import ma.hatim.ahadri.pricecrawler.fragment.category.CategoryListFragment;
import ma.hatim.ahadri.pricecrawler.model.Category;
import ma.hatim.ahadri.pricecrawler.service.CategoryService;

/**
 * Created by ahadri on 23/05/16.
 */
public class CategoryListActivity extends AppCompatActivity {

	private ViewPager viewPager;
	private TabLayout categoriesTabs;
	private String TAG = "Tabs Activity ";
	private List<Category> categories = new ArrayList<>();
	private CategoryService categoryService = CategoryService.getInstance();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_main_layout);

		// Setting the toolbar
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		categoriesTabs = (TabLayout) findViewById(R.id.categories);
		categoriesTabs.setEnabled(true);

		try {
		new GetCategories().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setupViewPager(ViewPager viewPager)
	{
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		CategoryListFragment listProductFragment;
		Bundle args;
		if (categories != null) {
			for (Category category : categories) {
				args = new Bundle();
				args.putSerializable("products", (Serializable) category.getProductList());
				listProductFragment = new CategoryListFragment();
				listProductFragment.setArguments(args);
				String categoryName = category.getCategoryName().toLowerCase().replace('_', ' ');
				adapter.addFragment(listProductFragment, categoryName);
			}
			viewPager.setAdapter(adapter);
		}
	}

	class ViewPagerAdapter extends FragmentStatePagerAdapter {
		private final List<CategoryListFragment> categoriesFragment = new ArrayList<>();
		private final List<String> categoriesTitleList = new ArrayList<>();


		private ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}


		@Override
		public CategoryListFragment getItem(int i) {
			CategoryListFragment listProductFragment = new CategoryListFragment();
			Bundle args = new Bundle();
			args.putSerializable("products", (Serializable) categories.get(i).getProductList());
			listProductFragment.setArguments(args);
			return listProductFragment;
		}


		@Override
		public int getCount() {
			return categoriesFragment.size();
		}

		private void addFragment(CategoryListFragment fragment, String title) {
			categoriesFragment.add(fragment);
			categoriesTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return categoriesTitleList.get(position);
		}

	}

	/**
	 * The Options Menu's Work
	 **/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.app_options_menu, menu);
		/** Responding to SearchView user interactions **/
		MenuItem searchItem = menu.findItem(R.id.menu_item_search);
		final SearchView searchView = (SearchView) searchItem.getActionView();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String s) {
				Intent intent = new Intent(getApplicationContext(), SearchProductActivity.class);
				intent.putExtra("query", s);
				startActivity(intent);
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}


	private class GetCategories extends AsyncTask<Void, Void, List<Category>> {


		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(CategoryListActivity.this);
			pDialog.setMessage("Connecting.....");
			pDialog.show();
		}
		@Override
		protected List<Category> doInBackground(Void... params) {
			return categoryService.getCategories();
		}

		@Override
		protected void onPostExecute(List<Category> res) {
			pDialog.dismiss();
			categories = res;
			// Setting the tabs
			viewPager = (ViewPager) findViewById(R.id.viewpager);
			setupViewPager(viewPager);
			categoriesTabs.setupWithViewPager(viewPager);

		}

	}
}