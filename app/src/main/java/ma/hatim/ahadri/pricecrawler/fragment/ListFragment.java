package ma.hatim.ahadri.pricecrawler.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.service.VolleyController;
import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.activity.product.ProductsPagerActivity;
import ma.hatim.ahadri.pricecrawler.model.Product;

/**
 * Created by hatim on 01/03/16.
 */


public abstract class ListFragment extends Fragment {

	protected  List<Product> products = new ArrayList<>();
	private final String TAG = this.getClass().getSimpleName();
	protected  abstract ArrayList<Product> getProducts();
	protected  abstract void setProducts(ArrayList<Product> products);
	private static final String PRODUCT_INDEX = "product_index";
	private static final String PRODUCTS = "products";

	/*
				Creating the Products Holder class
			 */
	protected class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		// The RecyclerView Items :
		private TextView productTitle;
		private TextView productPrice;
		private ImageView productImg;
		private ImageView supplier;
		private int product_position;

		public ProductHolder(View itemView) {
			super(itemView);
			itemView.setOnClickListener(this);

			productImg = (ImageView) itemView.findViewById(R.id.thumbnail);
			productTitle = (TextView) itemView.findViewById(R.id.product_title);
			productPrice = (TextView) itemView.findViewById(R.id.product_price);
			supplier = (ImageView) itemView.findViewById(R.id.product_supplier);
		}


		public void bindProduct(Product product, int position) {
			productTitle.setText(product.getTitle());
			productPrice.setText(product.getPrice());
			//Log.i(TAG, product.getImgUrl());
			ImageRequest request = new ImageRequest(
					product.getImgUrl(),
					new Response.Listener<Bitmap>() {
						@Override
						public void onResponse(Bitmap response) {
							productImg.setImageBitmap(response);
						}
					}, 0, 0, null,
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							productImg.setImageResource(R.drawable.ic_error_black_24dp);
						}
					}
			);

			VolleyController.getInstance().addToRequestQueue(request);
			switch (product.getSupplier()) {
				case "jumia":
					supplier.setImageResource(R.drawable.jumia);
					break;
				case "kaymu":
					supplier.setImageResource(R.drawable.kaymu);
					break;
			}
			product_position = position;
		}

		@Override
		public void onClick(View view) {
			// If an  item (Product) is pressed !
			Intent intent = new Intent(getActivity() , ProductsPagerActivity.class);
			intent.putExtra(PRODUCT_INDEX , product_position);
			intent.putExtra(PRODUCTS , (ArrayList<Product>) products);
			startActivity(intent);
		}
	}

    /*
        Creating the Products Adapter class
    */


	protected class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {
		private List<Product> products = new LinkedList<>();

		public ProductAdapter(List<Product> productItems) {
			products = productItems;

		}


		@Override
		public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			View view = inflater.inflate(R.layout.list_item_products, viewGroup, false);
			return new ProductHolder(view);
		}


		@Override
		public void onBindViewHolder(ProductHolder productHolder, int position) {
			Product product = products.get(position);
			productHolder.bindProduct(product, position);
		}

		@Override
		public int getItemCount() {
			return getProducts() == null ? 0 : products.size();
		}
	}

}