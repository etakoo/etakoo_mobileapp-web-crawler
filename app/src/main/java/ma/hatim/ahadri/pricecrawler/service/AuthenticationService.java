package ma.hatim.ahadri.pricecrawler.service;

import android.content.Context;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import ma.hatim.ahadri.pricecrawler.config.Config;
import ma.hatim.ahadri.pricecrawler.db.DBConfig;

/**
 * Created by ahadri on 25/12/16.
 */

public class AuthenticationService {

	private static  AuthenticationService authenticationService = new AuthenticationService();


	public static  AuthenticationService getInstance() {
			return authenticationService;
	}


	private final String TAG = getClass().getSimpleName();


	public boolean checkLogin(String email, String password, Context applicationContext) {
		OkHttpClient httpClient = new OkHttpClient();

		RequestBody args = new FormEncodingBuilder()
				.add("email", email)
				.add("psswd", password)
				.build();
		Request request = new Request.Builder()
				.url(Config.LOGIN)
				.post(args)
				.build();

		try {
			Response response = httpClient.newCall(request).execute();
			String responseData = response.body().string();
			if (response.code() == 200) {
				JSONObject responseJson = new JSONObject(responseData);
				// Adding the user to the local database , if it's not already added
				if (DBConfig.getDB(applicationContext).getUserInfo() == null) {
					String full_name = responseJson.getString("flname");
					DBConfig.getDB(applicationContext).addUserInfo(email, full_name);
				}
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean registerUser(String email, String full_name, String psswd, Context applicationContext) {
		OkHttpClient httpClient = new OkHttpClient();

		RequestBody args = new FormEncodingBuilder()
				.add("email", email)
				.add("psswd", psswd)
				.add("flname", full_name)
				.build();
		Request request = new Request.Builder()
				.url(Config.SIGNUP)
				.post(args)
				.build();

		try {
			Response response = httpClient.newCall(request).execute();
			if (response.code() == 200) {
				DBConfig.getDB(applicationContext).addUserInfo(email, full_name);
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	{

	}



}
