package ma.hatim.ahadri.pricecrawler.db;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import ma.hatim.ahadri.pricecrawler.model.User_Info;

/**
 * Created by ahadri on 24/05/16.
 */
public class DBWrapper extends CursorWrapper
{
    private String TAG = "Data Base Wrapper ";
    public DBWrapper(Cursor cursor)
    {
        super(cursor);
    }

    public User_Info getUserInfo()
    {
        if(this.getCount() == 0)
        {
            Log.i(TAG , "No uer found in the data base !");
            return null ;
        }
        else
        {
            User_Info user_info = new User_Info();
            try
            {
                this.moveToFirst();
                String full_name = getString(getColumnIndex(DBSchema.UserTable.Columuns.FULL_NAME));
                String email = getString(getColumnIndex(DBSchema.UserTable.Columuns.EMAIL));
                user_info.setFull_name(full_name);
                user_info.setEmail(email);
            }
            finally
            {
                this.close();
                Log.i(TAG, "User found : "+user_info.toString());
                return user_info;
            }
        }


    }
}
