package ma.hatim.ahadri.pricecrawler.config;

/**
 * Created by hatim on 07/04/16.
 */
public class Config
{
    /** API Search URL **/
    public static final String SEARCH = "https://e-takoo-api.herokuapp.com/products/search/" ;

    /** API Registration URL **/
    public static final String SIGNUP = "https://e-takoo-api.herokuapp.com/users/signup";
    /** API Login URL **/
    public static final String LOGIN = "https://e-takoo-api.herokuapp.com/users/login" ;

    /** API Recommendation(KMeans) URL **/
    public static final String RECOMMEND = "https://e-takoo-api.herokuapp.com/products/getall/";

    /** Get more details about a specific product **/
    public static final String PRODUCT_DETAILS = "https://e-takoo-api.herokuapp.com/products/details/" ;

}
