package ma.hatim.ahadri.pricecrawler.activity.search;

import android.support.v4.app.Fragment;
import ma.hatim.ahadri.pricecrawler.activity.MainActivity;
import ma.hatim.ahadri.pricecrawler.fragment.search.SearchProductsFragment;


/*
    The Main Activity class of the App after the Login and the registration activity of course :D !
 */
public class SearchProductActivity extends MainActivity
{
    @Override
    public Fragment createFragment()
    {
        return SearchProductsFragment.newInstance();
    }
}