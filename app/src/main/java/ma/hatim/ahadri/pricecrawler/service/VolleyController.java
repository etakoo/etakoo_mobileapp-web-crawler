package ma.hatim.ahadri.pricecrawler.service;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import ma.hatim.ahadri.pricecrawler.util.BitmapCache;

/**
 * Created by hatim on 07/04/16.
 */
public class VolleyController extends Application
{
    public static final String TAG = VolleyController.class.getSimpleName();
    private RequestQueue mRequestQueue ;
    private ImageLoader mImageLoader ;
    private static  VolleyController mInstance ;


    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this ;
    }


    public static synchronized  VolleyController getInstance()
    {
        return mInstance ;
    }


    public RequestQueue getRequestQueue()
    {
        if(mRequestQueue == null)
        {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue ;
    }


    public ImageLoader getImageLoader()
    {
        getRequestQueue();
        if(mImageLoader == null)
        {
            mImageLoader = new ImageLoader(this.mRequestQueue , new BitmapCache());
        }

        return this.mImageLoader ;
    }



    public <T> void addToRequestQueue(Request<T> request , String tag)
    {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(Request<T> request)
    {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }


    public void cancelPendingRequests(Object tag)
    {
        if(mRequestQueue != null)
        {
            mRequestQueue.cancelAll(tag);
        }
    }

}
