package ma.hatim.ahadri.pricecrawler.fragment.product;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.model.Product;
import ma.hatim.ahadri.pricecrawler.service.ProductService;

import static ma.hatim.ahadri.pricecrawler.R.id.imageButton;
import static ma.hatim.ahadri.pricecrawler.R.id.imageButton2;

/**
 * Created by hatim on 04/04/16.
 */
public class ProductFragment extends Fragment {
	private ProductService productService = ProductService.getInstance();
	private final String PRODUCT = "product";
	private final String TAG = this.getClass().getSimpleName();
	private Product product;
	private View view;
	private TextView productDescription;
	private TextView productLink;
	private TextView productTitle;
	private TextView productPrice;
	private ImageView productImage;
	private ImageView Supplier;
	private static int cptnext = 0;
	private static int cptback = 0;
	Bitmap bitmap;
	ProgressBar imgProgressBar;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		product = (Product) getArguments().getSerializable(PRODUCT);

			try {
				new GetProductDetails().execute(product.getSupplier(), product.getUrl());
			} catch (Exception e) {
				e.printStackTrace();
			}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.product_layout, container, false);

		productTitle = (TextView) view.findViewById(R.id.product_title);
		productTitle.setText(product.getTitle());
		productPrice = (TextView) view.findViewById(R.id.product_price);
		productPrice.setText(product.getPrice() + " Dhs");
		productLink = (TextView) view.findViewById(R.id.product_link);
		productLink.setText(Html.fromHtml("<a href=\"" + product.getUrl() + "\"> Check Online " + "</a>"));
		productLink.setLinksClickable(true);
		productLink.setMovementMethod(LinkMovementMethod.getInstance());
		imgProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		ImageButton button = (ImageButton) view.findViewById(imageButton);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (product.getBigImgUrl().size() > 0) {
					if (cptnext < product.getBigImgUrl().size()) {
						new LoadImage(productImage).execute(product.getBigImgUrl().get(cptnext));
						cptback = cptnext - 1;
						cptnext++;
					} else {
						cptnext = 0;
						cptback = product.getBigImgUrl().size();
						new LoadImage(productImage).execute(product.getBigImgUrl().get(cptnext));
					}
				}
			}
		});

		ImageButton button2 = (ImageButton) view.findViewById(imageButton2);
		button2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (product.getBigImgUrl().size() > 0) {
					if (cptback > 0) {

						new LoadImage(productImage).execute(product.getBigImgUrl().get(cptback));
						cptnext = cptback + 1;
						cptback--;

					} else {
						cptback = product.getBigImgUrl().size() - 1;
						new LoadImage(productImage).execute(product.getBigImgUrl().get(0));
						cptnext = 1;
					}
				}
			}
		});


		Supplier = (ImageView) view.findViewById(R.id.supplier);
		switch (product.getSupplier()) {
			case "kaymu":
				Supplier.setImageResource(R.drawable.kaymu);
				break;
			case "jumia":
				Supplier.setImageResource(R.drawable.jumia);
				break;
		}

		return view;
	}


	private class GetProductDetails extends AsyncTask<String, Void, HashMap<String, List<String>>> {

		@Override
		protected HashMap<String, List<String>> doInBackground(String... params) {
			return productService.getProductDetails(params[0], params[1]);
		}

		@Override
		protected void onPostExecute(HashMap<String, List<String>> productDetails) {
			product.setBigImgUrl(productDetails.get("imgs"));
			product.setDescription(productDetails.get("details"));
			setUpDetailsUI();
		}
	}

	private void setUpDetailsUI() {
		productImage = (ImageView) view.findViewById(R.id.product_image);
		new LoadImage(productImage).execute(product.getImgUrl());
		productDescription = (TextView) view.findViewById(R.id.product_description);
		productDescription.setText(formatArrayToString(product.getDescription()));
		productDescription.setMovementMethod(new ScrollingMovementMethod());
	}


	public String formatArrayToString( List<String> strings) {
		String descriptionString = "";
		for (String s: strings) {
			descriptionString += s + "\n";
		}
		return descriptionString == "" ? "No Description provided" : descriptionString;
	}


	private class LoadImage extends AsyncTask<String, String, Bitmap> {
		ImageView img;

		public LoadImage(ImageView productImage) {
			this.img = productImage;
		}

		@Override
		protected void onPreExecute() {
			imgProgressBar.setVisibility(View.VISIBLE); // show the ProgressBar
			super.onPreExecute();

		}

		protected Bitmap doInBackground(String... args) {
			try {
				bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return bitmap;
		}

		protected void onPostExecute(Bitmap image) {
			imgProgressBar.setVisibility(View.GONE); // show the ProgressBar
			img.setImageBitmap(image);
			int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, image.getWidth(), img.getResources().getDisplayMetrics());
			int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, image.getHeight(), img.getResources().getDisplayMetrics());
			img.setMinimumWidth(width);
			img.setMinimumHeight(height);


		}
	}
}