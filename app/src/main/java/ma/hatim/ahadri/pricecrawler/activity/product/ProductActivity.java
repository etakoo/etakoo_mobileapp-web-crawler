package ma.hatim.ahadri.pricecrawler.activity.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.activity.MainActivity;
import ma.hatim.ahadri.pricecrawler.activity.search.SearchProductActivity;
import ma.hatim.ahadri.pricecrawler.fragment.product.ProductFragment;
import ma.hatim.ahadri.pricecrawler.model.Product;

/**
 * Created by hatim on 04/04/16.
 */
public class ProductActivity extends MainActivity
{

	private static final String PRODUCT = "product";

	public static Fragment newIntent(Product product) {
		Bundle args = new Bundle();
		args.putSerializable(PRODUCT, product);
		Fragment productFragment = new ProductFragment();
		productFragment.setArguments(args);
		return productFragment;
	}

    @Override
    public Fragment createFragment()
    {
       Product product = (Product) getIntent().getSerializableExtra(PRODUCT);
	    Bundle args = new Bundle();
		args.putSerializable(PRODUCT, product);
		ProductFragment productFragment = new ProductFragment();
		productFragment.setArguments(args);
		return productFragment;
    }
}
