package ma.hatim.ahadri.pricecrawler.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ahadri on 24/05/16.
 */
public class DBHelper extends SQLiteOpenHelper
{
    private static final int VERSION = 1 ;
    private  final String TAG = getClass().getSimpleName();
    private static final String DATA_BASE_NAME = "userInfo";
    private static final String CREATE_DB = "create table "+
    DBSchema.UserTable.NAME + "("+
        DBSchema.UserTable.Columuns.ID + " integer primary key autoincrement , "+
            DBSchema.UserTable.Columuns.EMAIL + " varchar not null , "+
            DBSchema.UserTable.Columuns.FULL_NAME + " varchar not null"+
         ");";


    public DBHelper(Context context)
    {
        super(context , DATA_BASE_NAME , null , VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        Log.i(TAG , "Creating the data base !");
        database.execSQL(CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database , int oldVersion , int newVersion)
    {
        database.execSQL("drop table "+ DBSchema.UserTable.NAME);
        onCreate(database);
    }





}
