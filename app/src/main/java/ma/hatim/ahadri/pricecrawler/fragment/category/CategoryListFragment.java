package ma.hatim.ahadri.pricecrawler.fragment.category;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.List;


import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.fragment.ListFragment;
import ma.hatim.ahadri.pricecrawler.model.Product;
import ma.hatim.ahadri.pricecrawler.service.ProductService;

/**
 * Created by ahadri on 14/12/16.
 */

public class CategoryListFragment extends ListFragment{

	private final String TAG = this.getClass().getSimpleName();
	private RecyclerView productsRecyclerView ;
	private static ArrayList<Product> products = new ArrayList<>();

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setRetainInstance(true); // To resolve the landscape case , I know it's not the best solution for that !
		setHasOptionsMenu(true); // To add an Options Menu
		// Get the products
		products = (ArrayList<Product>) getArguments().getSerializable("products");
		setProducts(products);
	}

	public void setProducts(ArrayList<Product> products) {
		super.products = products;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}


	@Override
	public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.products_list , container , false);
		productsRecyclerView = (RecyclerView) view.findViewById(R.id.products_recycler_view);
		productsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		setRetainInstance(true);
		setUpAdapter();
		return productsRecyclerView;
	}
	/**     Test if the fragment is currently added to its activity.    **/

	private void setUpAdapter()
	{
		if(isAdded())
		{
			productsRecyclerView.setAdapter(new ProductAdapter(products));
		}
	}
}
