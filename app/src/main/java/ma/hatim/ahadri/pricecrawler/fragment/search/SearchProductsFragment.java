package ma.hatim.ahadri.pricecrawler.fragment.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.fragment.ListFragment;
import ma.hatim.ahadri.pricecrawler.model.Product;
import ma.hatim.ahadri.pricecrawler.service.ProductService;

/**
 * Created by ahadri on 14/12/16.
 */

public class SearchProductsFragment extends ListFragment{

	private RecyclerView productsRecyclerView ;
	private final String TAG = this.getClass().getSimpleName();
	private static ArrayList<Product> products = new ArrayList<>();
	private ProductService productService = ProductService.getInstance();
	private String userQuery = "";

	public static ListFragment newInstance()
	{
		return new SearchProductsFragment();
	}

	public ArrayList<Product> getProducts()
	{
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		super.products = products;
	}



	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true); // To add an Options Menu

		// Get the search query from the intent
		Intent intent = getActivity().getIntent();
		userQuery = intent.getStringExtra("query");

		try {
			products = (ArrayList<Product>) new GetProductsTask().execute().get();
			setProducts(products);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.products_list , container , false);
		productsRecyclerView = (RecyclerView) view.findViewById(R.id.products_recycler_view);
		productsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		setRetainInstance(true);
		setUpAdapter();
		return productsRecyclerView;
	}

	/**     Test if the fragment is currently added to its activity.    **/
	private void setUpAdapter()
	{
		if(isAdded())
		{
			productsRecyclerView.setAdapter(new ProductAdapter(products));
		}
	}



	private class GetProductsTask extends AsyncTask<Void, Void, List<Product>> {

		@Override
		protected List<Product> doInBackground(Void... params) {
			return productService.getSearchProducts(userQuery);
		}


		@Override
		protected void onPostExecute(List<Product> products) {
		}

	}


	/**  The Options Menu's Work **/

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		this.getActivity().getMenuInflater().inflate(R.menu.app_options_menu, menu);
		/** Responding to SearchView user interactions **/
		MenuItem searchItem = menu.findItem(R.id.menu_item_search);
		final SearchView searchView = (SearchView) searchItem.getActionView();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(String query)
			{
				userQuery = query;
				try {
					products = (ArrayList<Product>) new GetProductsTask().execute().get();
					setProducts(products);
					setUpAdapter(); // Notify the adapter
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText)
			{
				return false;
			}
		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return super.onOptionsItemSelected(item);
	}



}
