package ma.hatim.ahadri.pricecrawler.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahadri on 23/05/16.
 */
public class Category
{
    private String categoryName;
	private List<Product> productList = new ArrayList<>();

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Category() {
	}

	@Override
	public String toString() {
		return "Category{" +
				"categoryName='" + categoryName + '\'' +
				", productList=" + productList +
				'}';
	}

	public Category(String categoryName, List<Product> productList) {
	    this.categoryName = categoryName;
        this.productList = productList;
    }

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public void pushProducts(List<Product> products) {
		this.productList.addAll(products);
	}
}
