package ma.hatim.ahadri.pricecrawler.activity.authentication;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ma.hatim.ahadri.pricecrawler.activity.category.CategoryListActivity;
import ma.hatim.ahadri.pricecrawler.service.AuthenticationService;
import ma.hatim.ahadri.pricecrawler.util.SessionManager;
import ma.hatim.ahadri.pricecrawler.R;


public class RegisterActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
	private Boolean registrationStatus;
    private SessionManager session ;
    AuthenticationService authenticationService =AuthenticationService.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        // Initialize  the Session Manager
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn())
        {
            Intent intent = new Intent(RegisterActivity.this , CategoryListActivity.class);
            startActivity(intent);
            finish();
        }


        /** Register Button Click event **/
        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                String full_name = inputFullName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (!full_name.isEmpty() && !email.isEmpty() && !password.isEmpty())
                {
	                try {
		                new RegisterTask().execute(email, full_name, password);
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                } else
                {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

	private class RegisterTask extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(RegisterActivity.this);
			pDialog.setMessage("Connecting.....");
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			return authenticationService.registerUser(params[0] , params[1] , params[2], getApplicationContext());
		}

		@Override
		protected void onPostExecute(Boolean status) {
			pDialog.dismiss();
			registrationStatus = status;
			if(registrationStatus) {
				/** Lunch the Login Activity **/
				Toast.makeText(getApplicationContext() , "Account Successfully Created" , Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(RegisterActivity.this , LoginActivity.class);
				startActivity(intent);
			} else {
				Toast.makeText(getApplicationContext() , "Email Already exists!, Please try another one" , Toast.LENGTH_LONG).show();
			}
		}
	}

}
