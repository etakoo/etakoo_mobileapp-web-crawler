package ma.hatim.ahadri.pricecrawler.util;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by ahadri on 22/05/16.
 */
public class BitmapCache extends LruCache<String,Bitmap> implements ImageLoader.ImageCache
{

    public static int getDefaultCacheSize()
    {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8 ;

        return cacheSize ;
    }

    public BitmapCache()
    {
        this(getDefaultCacheSize());
    }

    public BitmapCache(int sizeInKiloBytes)
    {
        super(sizeInKiloBytes);
    }


    @Override
    protected int sizeOf(String key , Bitmap value)
    {
        return value.getRowBytes() * value.getHeight() / 1024 ;
    }


    @Override
    public Bitmap getBitmap(String url)
    {
        return get(url);
    }



    @Override
    public void putBitmap(String url , Bitmap bitmap)
    {
        put(url , bitmap);
    }
}
