package ma.hatim.ahadri.pricecrawler.activity.product;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ma.hatim.ahadri.pricecrawler.R;
import ma.hatim.ahadri.pricecrawler.fragment.product.ProductFragment;
import ma.hatim.ahadri.pricecrawler.model.Product;

/**
 * Created by hatim on 04/04/16.
 */
public class ProductsPagerActivity extends AppCompatActivity
{
    private ViewPager mViewPager ;
    private List<Product> products = new ArrayList<>();
    private static final String PRODUCT = "product";
	private static final String PRODUCT_INDEX = "product_index";
	private static final String PRODUCTS = "products";
    private static final String TAG = "ProductsPagerActivity" ;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productspager);

        final int product_index = (int) getIntent().getSerializableExtra(PRODUCT_INDEX);
        products = (ArrayList<Product>) getIntent().getSerializableExtra(PRODUCTS);

        mViewPager = (ViewPager) findViewById(R.id.products_pager);
        FragmentManager manager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(manager) {
            @Override
            public Fragment getItem(int position) {
			Bundle args = new Bundle();
	        Product product = products.get(position);
			args.putSerializable(PRODUCT, product);
			Fragment productFragment = ProductActivity.newIntent(product);
			productFragment.setArguments(args);
	        return productFragment;
            }

            @Override
            public int getCount() {
                return products.size();
            }
        });

        for(int i = 0 ; i < products.size() ; i++)
        {
            if( i == product_index)
            {
                mViewPager.setCurrentItem(product_index);
            }
        }
    }
}
