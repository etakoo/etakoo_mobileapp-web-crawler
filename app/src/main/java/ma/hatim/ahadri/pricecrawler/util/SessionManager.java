package ma.hatim.ahadri.pricecrawler.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


/**
 * Created by hatim on 13/04/16.
 */

public class SessionManager
{
    // Log Tag !
    private static String TAG = SessionManager.class.getSimpleName() ;

    // SharedPreferences
    SharedPreferences session ;
    Editor  sessionEditor ;
    Context _context ;

    // SharedPreferences Mode = 0 (PRIVATE_MODE)
    int PRIVATE_MODE = 0 ;

    // SharedPreferences file name
    private static final String sessionFileName = "etakko_user_session";

    private static final String IS_LOGGED_IN = "isLoggedIn" ;

    public SessionManager(Context context)
    {
        this._context = context ;
        session = _context.getSharedPreferences(sessionFileName , PRIVATE_MODE) ;
        sessionEditor = session.edit();
    }


    public void setLoggedIn(boolean isLoggedIn)
    {
            sessionEditor.putBoolean(IS_LOGGED_IN, isLoggedIn);
            sessionEditor.commit(); // Commit the changes !
    }


    public boolean isLoggedIn()
    {
        return session.getBoolean(IS_LOGGED_IN, false);
    }



}
