package ma.hatim.ahadri.pricecrawler.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hatim on 01/03/16.
 */

/*
    This class present the Model of a product
 */

public class Product implements Serializable
{

    private String title ;
    private List<String> description = new ArrayList<>(); // Product details
    private String price ;
    private String url ;
    private String supplier ;
    private String imgUrl ;
    private List<String> BigImgUrl = new ArrayList<>();

    public Product()
    {
        this.title = "";
        this.price = "";
        this.url = "";
        this.supplier = "";
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Product(String title, ArrayList<String> description, String price, String url, String supplier , String image)
    {

        this.title = title;
        this.description = description;
        this.price = price;
        this.url = url;
        this.supplier = supplier;
        this.imgUrl = image;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }


    public List<String> getBigImgUrl() {
        return BigImgUrl;
    }

    public void setBigImgUrl(List<String> bigImgUrl) {
        BigImgUrl = bigImgUrl;
    }

	@Override
	public String toString() {
		return "Product{" +
				"title='" + title + '\'' +
				", description='" + description + '\'' +
				", price='" + price + '\'' +
				", url='" + url + '\'' +
				", supplier='" + supplier + '\'' +
				", imgUrl='" + imgUrl + '\'' +
				", BigImgUrl=" + BigImgUrl +
				'}';
	}
}
