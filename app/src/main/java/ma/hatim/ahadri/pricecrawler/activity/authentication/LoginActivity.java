package ma.hatim.ahadri.pricecrawler.activity.authentication;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ma.hatim.ahadri.pricecrawler.activity.category.CategoryListActivity;
import ma.hatim.ahadri.pricecrawler.service.AuthenticationService;
import ma.hatim.ahadri.pricecrawler.util.SessionManager;
import ma.hatim.ahadri.pricecrawler.R;

public class LoginActivity extends Activity {
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private SessionManager session ;
    private final String TAG = getClass().getSimpleName();
    private AuthenticationService authenticationService = AuthenticationService.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Initialize  the Session Manager
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn())
        {
            Intent intent = new Intent(LoginActivity.this , CategoryListActivity.class);
            startActivity(intent);
            finish();
        }



        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
	                try {
		                new LoginTask().execute(email, password);
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
                 else
                {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        /** Link to Register Screen **/
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

	private class LoginTask extends AsyncTask<String, Void, Boolean> {

		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Connecting.....");
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			return authenticationService.checkLogin(params[0], params[1], getApplicationContext());
		}

		@Override
		protected void onPostExecute(Boolean loginStatus) {
			pDialog.dismiss();
			if (loginStatus) {
				// Activate the User's session !
				session.setLoggedIn(true);
				Toast.makeText(getApplicationContext(),
						"Account Successfully Created", Toast.LENGTH_SHORT)
						.show();
				/** Lunch the Login Activity **/
				Intent intent = new Intent(LoginActivity.this, CategoryListActivity.class);
				startActivity(intent);
			} else {
				Toast.makeText(getApplicationContext(),
						"Invalid Email Or Password, Please Try Again!", Toast.LENGTH_LONG)
						.show();
			}
		}
	}
}