package ma.hatim.ahadri.pricecrawler.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import ma.hatim.ahadri.pricecrawler.model.User_Info;

/**
 * Created by ahadri on 24/05/16.
 */
public class DBConfig
{
    private static DBConfig DBConfig;
    private Context context ;
    private SQLiteDatabase dataBase ;
    private String TAG = "In Data Base Manager";


    public static DBConfig getDB(Context context)
    {
        if(DBConfig == null)
        {
            DBConfig = new DBConfig(context);
        }
        return DBConfig;
    }

    private DBConfig(Context context)
    {
        this.context = context.getApplicationContext();
        dataBase = new DBHelper(context).getWritableDatabase();
    }

    public User_Info getUserInfo()
    {
        Cursor cursor = dataBase.query(
                        DBSchema.UserTable.NAME ,
                        null , // To get all the columns
                        null ,
                        null ,
                        null ,
                        null ,
                        null
                    );
        return new DBWrapper(cursor).getUserInfo();
    }

    public void addUserInfo(String email, String full_name)
    {
        ContentValues info = getContentValues(email, full_name);
        dataBase.insert(DBSchema.UserTable.NAME , null , info);
        Log.i(TAG, "User Info Added to Data Base!");
    }


    public void updateUserInfo(String email, String full_name)
    {
        ContentValues info = getContentValues(email, full_name);
        dataBase.update(DBSchema.UserTable.NAME , info , DBSchema.UserTable.Columuns.EMAIL +  "=  ?",new String[]{email});
        Log.i(TAG, "User info updated in the data base !");
    }


    public void deleteUserInfo(String email)
    {
        dataBase.delete(DBSchema.UserTable.NAME , DBSchema.UserTable.Columuns.FULL_NAME + "= ?",new String[]{email});
        Log.i(TAG , "User Info removed from Data base !");
    }


    private static ContentValues getContentValues(String email, String full_name)
    {
        ContentValues info = new ContentValues();
	    info.put(DBSchema.UserTable.Columuns.EMAIL , email);
	    info.put(DBSchema.UserTable.Columuns.FULL_NAME , full_name);
	    Log.i("DB", "===== >" +info.toString());
        return info ;
    }



























}
