package ma.hatim.ahadri.pricecrawler.model;

/**
 * Created by ahadri on 24/05/16.
 */
public class User_Info
{
    private String id ;
    private String full_name ;
    private String email ;


    public User_Info() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User_Info(String full_name, String email)
    {
        this.full_name = full_name;
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
