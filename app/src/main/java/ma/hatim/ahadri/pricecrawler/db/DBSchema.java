package ma.hatim.ahadri.pricecrawler.db;

/**
 * Created by ahadri on 24/05/16.
 */
public class DBSchema
{
    public static final class UserTable
    {
        public static final String NAME = "user";

        public  static  final class Columuns
        {
            public static final String ID = "id" ;
            public static final String FULL_NAME = "full_name";
            public static final String EMAIL = "email" ;
        }
    }
}
